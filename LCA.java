interface LCA {
    /**
     * Lowest Common Ancestor
     */
    int getlca(int v1, int v2);
}