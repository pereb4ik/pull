import java.util.*;

import static java.lang.Math.*;

public class RMQLCA implements LCA {
    /**
     * LCA on SparceTable
     */
    ArrayList<Integer>[] g;
    int n;
    int t = 0;
    int list[];
    int right[][];
    int first[];
    int depth[];

    /**
     * first[] - first including element
     */
    RMQLCA(ArrayList<Integer> g1[]) {
        g = g1;
        n = g.length;
        list = new int[n * 2 - 1];
        first = new int[n];
        depth = new int[n];
        dfs(0, 0);
        buildST();
    }

    void dfs(int v, int p) {
        first[v] = t;
        depth[v] = depth[p] + 1;
        list[t++] = v;
        for (int i = 0; i < g[v].size(); i++) {
            int u = g[v].get(i);
            if (u != p) {
                dfs(u, v);
                list[t++] = v;
            }
        }
    }

    /**
     * min by depth
     */
    int mind(int a, int b) {
        if (depth[a] < depth[b]) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * build Sparce Tables
     */
    void buildST() {
        int k = (int) (floor(log(list.length) / log(2))) + 1;
        /**
         * Max k that 2^k <= n
         */
        right = new int[list.length][k];
        int last = list.length - 1;
        for (int i = 0; i < k; i++) {
            right[last][i] = list[last];
        }
        for (int i = list.length - 2; i > -1; i--) {
            right[i][0] = list[i];
            for (int j = 1; j < k; j++) {
                right[i][j] = right[i][j - 1];
                int index = i + (1 << (j - 1));
                if (index < list.length) {
                    right[i][j] = mind(right[index][j - 1], right[i][j - 1]);
                }
            }
        }
    }

    int lca(int l, int r) {
        /**
         * k - max k that 2^k <= (size of segment(which = r - l + 1))
         */
        int k = (int) floor(log(r - l + 1) / log(2));
        int index = r - (1 << k) + 1;
        return mind(right[l][k], right[index][k]);
    }

    public int getlca(int v1, int v2) {
        return lca(min(first[v1], first[v2]), max(first[v1], first[v2]));
    }
}